
#import "Serializable.h"

/// @brief Base class for Analytics events that can be serialized to JSON.
@interface Ncp_Types_EventSerializable: Ncp_Types_Serializable

/// @brief The type of event.
/// This contains the schema $id from the JSON schema.
/// This is a required field.
@property ( strong, nonatomic, readonly ) NSString * eventType;

/// @brief Constructor
/// @param [in] schemaId The schema $id from the JSON schema.
/// @throws IllegalArgumentException if schemaId is null or empty.
- ( id ) initWithSchemaId:( NSString * ) schemaId;

/// @brief Constructor
/// @param [in] schemaId The schema $id from the JSON schema.
/// @param [in] json JSONObject to deserialize.
/// @throws IllegalArgumentException if schemaId is null or empty.
/// @throws SchemaViolationException if the JSON eventType doesn't match schemaId.
- ( id ) initWithSchemaId:( NSString * ) schemaId json:( NSDictionary * ) json;

/// Serialize this object to JSON.
/// @return JSON dictionary representing this object.
/// @throws SchemaViolationException if any required fields are missing.
- ( NSMutableDictionary * ) toJSON;

@end


#import <Foundation/Foundation.h>

/// @brief Base class for objects that can be serialized to JSON.
@interface Ncp_Types_Serializable: NSObject

/// @brief The original JSON object that this class was deserialized from.
/// Changing any fields in objects inheriting this class will NOT change this original JSON.
@property ( strong, nonatomic, readonly ) NSDictionary * originalJson;

/// @brief Constructor
/// @param [in] json The original JSON dictionary to keep a reference to.
///                  Changing any fields in objects inheriting this class will NOT change the original JSON.
/// @return The pointer to this object
/// @throws SchemaViolationException if any required fields are missing, or have the wrong type
- ( id ) initWithJson:( NSDictionary * ) json;

/// @brief Serialize this object to a JSON dictionary.
/// Default implementation always returns a new empty dictionary.
/// This returns a mutable dictionary for convenience of adding more key/values.
/// @return JSON dictionary representing this object.
///         Note: this has nothing to do with the original JSON object this class was deserialized from.
/// @throws SchemaViolationException if any required fields are missing.
- ( NSMutableDictionary * ) toJSON;

/// @brief Checks whether this object is valid, without throwing any exceptions.
/// If this object is valid, that means it conforms to the original JSON schema.
/// Default implementation always returns YES
/// @return Whether this object is valid.
- ( BOOL ) isValid;

@end


#pragma once

/// @brief Indicates that the operation was successful
#define NCP_RET_CODE_OK                  0

/// @brief Indicates that there was an internal error
#define NCP_RET_CODE_INTERNAL_ERROR      -1

/// @brief Indicates that one of the arguments passed was invalid.
#define NCP_RET_CODE_INVALID_ARG         -2

/// @brief Indicates that the configuration is invalid.
#define NCP_RET_CODE_INVALID_CONFIG      -3

/// @brief Indicates that the library has not been initialized properly.
#define NCP_RET_CODE_NOT_INITIALIZED     -4

/// @brief Indicates that there was a network error timeout and fetching data was aborted.
#define NCP_RET_CODE_NETWORK_ERROR       -5

/// @brief Indicates that there was an error in received payload.
#define NCP_RET_CODE_PROTOCOL_ERROR      -6

/// @brief Indicates that there was filesystem-related error.
#define NCP_RET_CODE_FILESYSTEM_ERROR    -7

/// @brief Indicates that there was memory-related error.
#define NCP_RET_CODE_MEMORY_ERROR        -8

/// @brief Indicates that there was a timeout and fetching data was aborted.
#define NCP_RET_CODE_TIMEOUT             -9

/// @brief Indicates that there was an error running the request (for example server rejected it).
#define NCP_RET_REQ_FAILED               -10

#ifndef PRAV_PUBLIC_API

#if ( defined( __unix__ ) || defined( __APPLE__ ) ) && ( defined( __GNUC__ ) || defined( __clang__ ) )
#define PRAV_PUBLIC_API    __attribute__( ( __visibility__ ( "default" ) ) )
#elif defined( WIN32 ) && defined( BUILDING_DLL )
#define PRAV_PUBLIC_API    __declspec ( dllexport )
#elif defined( WIN32 )
#define PRAV_PUBLIC_API    __declspec ( dllimport )
#else
#define PRAV_PUBLIC_API
#endif
#endif

#ifdef __cplusplus
extern "C" {
#endif

/// @brief Contains configuration for the NCP Policy client.
struct NcpPolicyClientConfig
{
    /// @brief Path to a directory where the local data will be stored.
    /// Required.
    const char * storageDir;

    /// @brief Base URL of the policy server to be used.
    /// Required.
    /// For example: "https://wam.dev.ctechdemo.com/api"
    const char * url;
};

/// @brief Initializes and configures NCP Policy client.
/// This function must be called before the client requests policies or hotspots from the server.
/// It is safe to call this function multiple times, for example if the configuration needs to be changed.
/// @param [in] config The configuration to set. A deep copy of configuration will be created,
///                    so it is not required to keep the values around after calling this function.
/// @param [in] subscriberId The unique ID of the subscriber.
/// @param [in] subscriberJson The subscriber information, as a JSON string.
/// @return One of NCP_RET_CODE_* codes.
PRAV_PUBLIC_API int ncpConfigurePolicyClient (
        struct NcpPolicyClientConfig * config, const char * subscriberId, const char * subscriberJson );

/// @brief Pushes current policy subscriber's data to the server.
/// If the data has not changed since the last push, this won't do anything.
/// This function will block until it completes the operation, encounters an error or times out.
/// It will also perform registration with the policy server, if required.
/// @param [in] timeout The max amount of time (in seconds) this function will block for.
///                     If timeout <= 0: blocks until it completes (or fails).
///                     If timeout > 0: blocks with a maximum timeout.
/// @return One of NCP_RET_CODE_* codes.
PRAV_PUBLIC_API int ncpPushPolicySubscriber ( int timeout );

/// @brief Returns cached policy data.
/// Policy data will be stored in a dynamically allocated memory.
/// The data is returned as a null-terminated string that contains a JSON array,
/// with each entry representing a single policy.
/// This function will not block and simply return whatever data is cached,
/// even if there is no data at all.
/// @param [out] data The location to store the pointer to policy data. This must be a valid pointer.
///                   If there is no cached policy data, this pointer will be set to NULL.
///                   It is caller's responsibility to free this data.
/// @return One of NCP_RET_CODE_* codes.
PRAV_PUBLIC_API int ncpGetCachedPolicies ( char ** data );

/// @brief Downloads current policies from the server.
/// Policy data will be stored in a dynamically allocated memory.
/// The data is returned as a null-terminated string that contains a JSON array,
/// with each entry representing a single policy.
/// This function will block until it completes the operation, encounters an error or times out.
/// It will also perform registration and push current subscriber data to the policy server, if required.
/// @param [out] data The location to store the pointer to policy data. This must be a valid pointer.
///                   If there is no cached policy data, this pointer will be set to NULL.
///                   It is caller's responsibility to free this data.
/// @param [in] timeout The max amount of time (in seconds) this function will block for.
///                     If timeout <= 0: blocks until it completes (or fails).
///                     If timeout > 0: blocks with a maximum timeout. If that happens, no policies are updated.
/// @return One of NCP_RET_CODE_* codes.
PRAV_PUBLIC_API int ncpFetchPolicies ( char ** data, int timeout );

/// @brief Returns cached hotspots data.
/// Hotspots data will be stored in a dynamically allocated memory.
/// The data is returned as a null-terminated string that contains a JSON array,
/// with each entry representing a single hotspot entry.
/// This function will not block and simply return whatever data is cached,
/// even if there is no data at all.
/// @param [out] data The location to store the pointer to hotspots data. This must be a valid pointer.
///                   If there is no cached hotspots data, this pointer will be set to NULL.
///                   It is caller's responsibility to free this data.
/// @return One of NCP_RET_CODE_* codes.
PRAV_PUBLIC_API int ncpGetCachedHotspots ( char ** data );

/// @brief Downloads current hotspots from the server.
/// Hotspots data will be stored in a dynamically allocated memory.
/// The data is returned as a null-terminated string that contains a JSON array,
/// with each entry representing a single hotspot entry.
/// This function will block until it completes the operation, encounters an error or times out.
/// It will also perform registration and push current subscriber data to the policy server, if required.
/// @param [out] data The location to store the pointer to hotspots data. This must be a valid pointer.
///                   If there is no cached hotspots data, this pointer will be set to NULL.
///                   It is caller's responsibility to free this data.
/// @param [in] timeout The max amount of time (in seconds) this function will block for.
///                     If timeout <= 0: blocks until it completes (or fails).
///                     If timeout > 0: blocks with a maximum timeout. If that happens, no hotspots are updated.
/// @return One of NCP_RET_CODE_* codes.
PRAV_PUBLIC_API int ncpFetchHotspots ( char ** data, int timeout );

#ifdef __cplusplus
}
#endif

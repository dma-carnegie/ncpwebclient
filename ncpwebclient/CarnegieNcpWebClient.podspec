lib_name = 'NcpWebClient'

Pod::Spec.new do |spec|
  spec.name     = 'CarnegieNcpWebClient'
  spec.version  = '0.0.6'
  spec.author   = 'Carnegie Technologies'
  spec.license  = 'Commercial'
  spec.homepage = 'https://www.carnegietechnologies.com/'
  spec.source   = { :git => 'https://gitlab.com/dma-carnegie/ncpwebclient.git',
                    :tag => "#{spec.version}" }
  spec.summary  = 'NCP web client module'
  spec.platform = :ios, '7.0'

  spec.source_files        = "#{lib_name}/**/*.h"
  spec.public_header_files = "#{lib_name}/**/*.h"
  spec.header_dir          = "#{lib_name}"
  spec.vendored_libraries  = "#{lib_name}/lib/libIos#{lib_name}.a", "#{lib_name}/lib/lib#{lib_name}.a"
  spec.preserve_paths      = "#{lib_name}/lib/*.a"
  spec.libraries           = "Ios#{lib_name}", "#{lib_name}"

  # Preserve directories in the include dir.
  # This is needed so the headers directory structure is not flattened,
  # so we can #import "ncp/types/Serializable.h" instead of <NcpWebClient/Serializable.h>.
  spec.header_mappings_dir = "#{lib_name}/include"

  # Add the CocoaPod public header dir to the header search paths.
  # This also needed so we can #import "ncp/types/Serializable.h" instead of <NcpWebClient/ncp/types/Serializable.h>.
  spec.xcconfig = { 'HEADER_SEARCH_PATHS' => "${PODS_ROOT}/Headers/Public/#{spec.name}/#{lib_name}" }

  spec.dependency 'CarnegiePravCore', '>= 0.0.4'
  spec.dependency 'CarnegieSimpleLog', '>= 0.0.1'
end
